const { CSSLoader } = require('./webpack/loaders/loaders.prod')
const {
    mediaQueryPlugin, miniCssExtractPlugin, removePlugin, spriteLoaderPlugin,
} = require('./webpack/plugins/plugins.prod')

module.exports = {
    mode: 'production',
    output: {
        filename: 'js/[name].[fullhash].min.js',
    },
    module: {
        rules: [
            CSSLoader,
        ],
    },
    plugins: [
        miniCssExtractPlugin,
        removePlugin,
        spriteLoaderPlugin,
        mediaQueryPlugin,
    ],
}
