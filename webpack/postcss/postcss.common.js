const plugins = [
    require('postcss-easy-import'),
    require('postcss-advanced-variables')({
        disable: '@import',
    }),
    require('postcss-nested'),
    require('postcss-url'),
    require('postcss-color-function'),
    require('postcss-flexbugs-fixes'),
    require('postcss-calc')({
        mediaQueries: true,
    }),
    require('postcss-preset-env')({
        browsers: 'last 2 versions',
    }),
    require('cssnano')({
        preset: ['advanced', {
            discardUnused: false,
            zindex: false,
        }],
    }),
]


module.exports = { plugins }
