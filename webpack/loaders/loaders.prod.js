const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const MediaQueryPlugin = require('media-query-plugin')
const postcssCommon = require('../postcss/postcss.common')
const postcssPrd = require('../postcss/postcss.prod')

const CSSLoader = {
    test: /\.css$/,
    use: [
        MiniCssExtractPlugin.loader,
        {
            loader: 'css-loader',
            options: {
                sourceMap: true,
                url: false,
            },
        },
        MediaQueryPlugin.loader,
        {
            loader: 'postcss-loader',
            options: {
                postcssOptions: {
                    plugins: [
                        ...postcssPrd.plugins,
                        ...postcssCommon.plugins,
                    ],
                },
            },
        },
    ],
}

module.exports = {
    CSSLoader,
}
