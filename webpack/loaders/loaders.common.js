const imagesLoader = {
    test: /\.(png|ttf|woff|eot|svg|jpe?g|gif)$/,
    use: [
        {
            loader: 'file-loader',
            options: {
                outputPath: 'vendor-images',
                publicPath: '../vendor-images',
                name: '[name].[ext]',
            },
        },
    ],
}

module.exports = {
    imagesLoader,
}
