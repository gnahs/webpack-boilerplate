const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const MediaQueryPlugin = require('media-query-plugin')
const postcssCommon = require('../postcss/postcss.common')


const CSSLoader = {
    test: /\.css$/,
    use: [
        MiniCssExtractPlugin.loader,
        {
            loader: 'css-loader',
            options: {
                sourceMap: true,
                url: false
            },
        },
        {
            loader: 'postcss-loader',
            options: {
                postcssOptions: {
                    plugins: postcssCommon.plugins,
                },
            },
        },
    ],
}

module.exports = {
    CSSLoader,
}
