const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ESLintPlugin = require('eslint-webpack-plugin')
const Stylelint = require('stylelint-webpack-plugin')
const LiveReloadPlugin = require('webpack-livereload-plugin')
const path = require('path')

const PROJECT_DIRECTORY = process.cwd()

const miniCssExtractPlugin = new MiniCssExtractPlugin({
    filename: 'css/[name].css',
})

const eslintWebpackPluguin = new ESLintPlugin({
    context: path.resolve(PROJECT_DIRECTORY, './resources/js'),
    extensions: [ '.js' ],
    exclude: [ 'node_modules', path.resolve(PROJECT_DIRECTORY, './public') ],
    fix: true,
    emitError: true,
    emitWarning: false,
})

// Don't use stylelint why they arae conflict with edia-query-plugin
const styleLintPlugin = new Stylelint({
    configFile: path.resolve(__dirname, '../../stylelint.config.js'),
    context: path.resolve(PROJECT_DIRECTORY, './resources/css'),
    files: '**/*.css',
    exclude: [ 'node_modules', path.resolve(PROJECT_DIRECTORY, './public') ],
    lintDirtyModulesOnly: true,
    failOnError: false,
    emitError: true,
    emitWarning: false,
    quiet: false,
})

const liveReloadPlugin = new LiveReloadPlugin({
    hostname: process.env.APP_URL || 'http://localhost',
})

module.exports = {
    styleLintPlugin,
    miniCssExtractPlugin,
    eslintWebpackPluguin,
    liveReloadPlugin,
}
