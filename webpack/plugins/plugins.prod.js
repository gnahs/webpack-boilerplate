const MediaQueryPlugin = require('media-query-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const RemovePlugin = require('remove-files-webpack-plugin')
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin')

const miniCssExtractPlugin = new MiniCssExtractPlugin({
    filename: 'css/[name].[hash].min.css',
})

const removePlugin = new RemovePlugin({
    before: {
        include: [
            './public/js',
            './public/css',
        ],
    },
})

const mediaQueryPlugin = new MediaQueryPlugin({
    include: true,
    queries: {
        'screen and (min-width: 768px)': 'medium-devices',
        '(min-width: 768px)': 'medium-devices',
        'screen and (min-width: 48rem)': 'medium-devices',
        '(min-width: 48rem)': 'medium-devices',

        'screen and (min-width: 1024px)': 'medium-devices-landscape',
        '(min-width: 1024px)': 'medium-devices-landscape',
        'screen and (min-width: 64rem)': 'medium-devices-landscape',
        '(min-width: 64rem)': 'medium-devices-landscape',

        'screen and (min-width: 1200px)': 'large-devices',
        '(min-width: 1200px)': 'large-devices',
        'screen and (min-width: 75rem)': 'large-devices',
        '(min-width: 75rem)': 'large-devices',

        'screen and (min-width: 1440px)': 'extra-large-devices',
        '(min-width: 1440px)': 'extra-large-devices',
        'screen and (min-width: 90rem)': 'extra-large-devices',
        '(min-width: 90rem)': 'extra-large-devices',
        'screen and (min-width: 1920px)': 'extra-large-devices',
        '(min-width: 1920px)': 'extra-large-devices',
        'screen and (min-width: 120rem)': 'extra-large-devices',
        '(min-width: 120rem)': 'extra-large-devices',
    },
})

const spriteLoaderPlugin = new SVGSpritemapPlugin('resources/icons/**/*.svg', {
    input: {
        allowDuplicates: false,
    },
    output: {
        filename: 'icons/icons-sprite.svg',
        svgo: {
            plugins: [
                {
                    name: 'removeAttrs',
                    params: {
                        attrs: '(clip-path)',
                    },
                },
            ],
        },
    },
    sprite: {
        prefix: 'sprite-icon-',
    },
})

module.exports = {
    mediaQueryPlugin,
    miniCssExtractPlugin,
    removePlugin,
    spriteLoaderPlugin,
}
