const { WebpackManifestPlugin } = require('webpack-manifest-plugin')

const webpackManifestPlugin = new WebpackManifestPlugin({
    publicPath: '',
})

module.exports = {
    webpackManifestPlugin,
}
