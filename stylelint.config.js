module.exports = {
    plugins: [
        'stylelint-scss',
        'stylelint-order',
    ],
    extends: [
        'stylelint-config-hudochenkov/order',
    ],
    rules: {
        'at-rule-no-unknown': null,
        'scss/at-rule-no-unknown': true,
        'scss/dollar-variable-pattern': /[a-zA-Z]-.+/,
        'scss/selector-no-redundant-nesting-selector': true,
        'scss/dollar-variable-colon-space-after': 'always',
        'scss/double-slash-comment-inline': "never",
        'scss/operator-no-unspaced': true,

        'block-no-empty': true,
        'color-no-invalid-hex': true,
        'comment-no-empty': true,

        'declaration-block-no-duplicate-properties': true,

        'font-family-no-duplicate-names': true,
        'font-family-no-missing-generic-family-keyword': true,
        'declaration-block-no-shorthand-property-overrides': true,

        'function-calc-no-unspaced-operator': true,
        'function-linear-gradient-no-nonstandard-direction': true,
        'declaration-block-no-duplicate-custom-properties': true,

        'keyframe-declaration-no-important': true,
        'media-feature-name-no-unknown': true,
        'shorthand-property-no-redundant-values': true,

        'no-descending-specificity': true,
        'no-duplicate-at-import-rules': true,
        'no-duplicate-selectors': true,
        'no-empty-source': true,
        'no-extra-semicolons': true,

        'length-zero-no-unit': true,
        'number-max-precision': 4,
        'selector-max-compound-selectors': 3,
        'selector-max-empty-lines': 0,
        'number-leading-zero': 'never',
        'function-whitespace-after': 'always',
        'declaration-colon-space-before': 'never',
        'declaration-colon-space-after': 'always',


        'block-opening-brace-newline-after': 'always',
        'block-closing-brace-newline-before': 'always',
        'block-closing-brace-empty-line-before': 'never',
        'block-opening-brace-space-before': 'always',

        'selector-pseudo-class-no-unknown': true,
        'selector-pseudo-element-no-unknown': true,
        'selector-type-no-unknown': true,
        'named-grid-areas-no-invalid': true,

        'string-no-newline': true,
        'unit-no-unknown': true,
        'max-nesting-depth': 3,

        'declaration-block-semicolon-newline-after': 'always',
        'declaration-block-semicolon-space-before': 'never',
        'declaration-block-trailing-semicolon': 'always',
        'max-empty-lines': 2,
        'no-irregular-whitespace': true,
        'no-missing-end-of-source-newline': true,
        indentation: 2,
    },
}
