const { miniCssExtractPlugin, liveReloadPlugin } = require('./webpack/plugins/pluguins.dev')
const { CSSLoader } = require('./webpack/loaders/loaders.dev')


module.exports = {
    mode: 'development',
    devtool: 'eval-source-map',
    stats: 'normal',
    module: {
        rules: [
            // loaders.IMAGESLoader,
            CSSLoader,
        ],
    },
    plugins: [
        miniCssExtractPlugin,
        liveReloadPlugin,
    ],
}
