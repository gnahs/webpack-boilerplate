const { merge } = require('webpack-merge')
const webpackCommon = require('./webpack.common')
const webpackDevelop = require('./webpack.dev')
const webpackProduction = require('./webpack.prod')

const environment = (process.env.NODE_ENV || 'development').trim()
if (environment === 'development') {
    module.exports = merge(webpackCommon, webpackDevelop, {
        entry: {
            home: 'js/home.js'
        }
    })
} else {
    module.exports = merge(webpackCommon, webpackProduction)
}
