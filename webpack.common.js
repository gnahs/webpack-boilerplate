const webpack = require('webpack')
const path = require('path')
const { ESBuildMinifyPlugin } = require('esbuild-loader')
const { imagesLoader } = require('./webpack/loaders/loaders.common')
const { webpackManifestPlugin } = require('./webpack/plugins/plugins.common')

const PROJECT_DIRECTORY = process.cwd()

require('dotenv')
    .config({ path: path.resolve(PROJECT_DIRECTORY, './.env') })


// eslint-disable-next-line no-new
new webpack.DefinePlugin({
    'process.env': JSON.stringify(process.env),
})

module.exports = {
    stats: 'minimal',
    output: {
        filename: 'js/[name].js',
        chunkFilename: 'js/chunks/[chunkhash].chunk.js',
        path: path.resolve(PROJECT_DIRECTORY, './public/'),
    },
    resolve: {
        alias: {
            js: path.resolve(PROJECT_DIRECTORY, './resources/js'),
            css: path.resolve(PROJECT_DIRECTORY, './resources/css'),
        },
    },
    module: {
        rules: [
            imagesLoader,
        ],
    },
    plugins: [
        webpackManifestPlugin,
    ],
    optimization: {
        minimizer: [
            new ESBuildMinifyPlugin({
                target: 'esnext',
                css: true,
            }),
        ],
    },
}
